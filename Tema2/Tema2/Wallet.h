#pragma once
#include <chrono>
#include <thread>
#include <atomic>
#include <iostream>

class Wallet
{
public:
	Wallet();
	~Wallet();
public:
	void setMoney(int amount);
	int getMoney() const;
	void addMoney(const int sum);
private:
	int money = 0;
};

