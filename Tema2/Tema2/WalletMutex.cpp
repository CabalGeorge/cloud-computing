#include "WalletMutex.h"

WalletMutex::WalletMutex()
{
}

WalletMutex::~WalletMutex()
{
}

void WalletMutex::setMoney(int amount)
{
	this->money = amount;
}

int WalletMutex::getMoney() const
{
	return this->money;
}

void WalletMutex::addMoney(const int arg)
{
	for (int i = 0; i < arg; ++i)
	{
		// Using Lockguard
		std::lock_guard<std::mutex> lck(mtx);

		// Using Mutex
		// mtx.lock();

		// Heavy calculation simulation
		std::chrono::milliseconds time(1000);
		std::this_thread::sleep_for(time);


		// Money++
		this->money++;
		std::cout << "Worker Thread " << std::this_thread::get_id() << " is adding 1 to " << this->getMoney() << std::endl;
		//mtx.unlock();
	}
}
