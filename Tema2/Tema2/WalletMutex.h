#pragma once
#include <chrono>
#include <thread>
#include <mutex>
#include <iostream>

class WalletMutex
{
public:
	WalletMutex();
	~WalletMutex();
public:
	void setMoney(int amount);
	int getMoney() const;
	void addMoney(const int sum);
private:
	std::mutex mtx;
	int money = 0;
};