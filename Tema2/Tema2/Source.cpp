#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <future>
#include "Wallet.h"
#include "WalletMutex.h"

void RaceConditionProblem()
{
	Wallet myWallet;
	std::vector<std::thread> arrThreads;

	for (auto i = 0; i < 100; ++i)
		arrThreads.push_back(std::thread(&Wallet::addMoney, &myWallet, 5));

	std::for_each(std::begin(arrThreads), std::end(arrThreads), std::mem_fn(&std::thread::join));
	std::cout << "Value of the wallet = " << myWallet.getMoney() << std::endl;
}

void RaceConditionSolution()
{
	WalletMutex myWallet;
	std::vector<std::thread> arrThreads;

	for (auto i = 0; i < 100; ++i)
		arrThreads.push_back(std::thread(&WalletMutex::addMoney, &myWallet, 5));


	std::for_each(std::begin(arrThreads), std::end(arrThreads), std::mem_fn(&std::thread::join));
	std::cout << "Value of the wallet = " << myWallet.getMoney() << std::endl;
}

int main()
{
	RaceConditionProblem();
	RaceConditionSolution();

	std::cin.get();
	return 1;

}