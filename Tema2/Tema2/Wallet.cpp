#include "Wallet.h"

Wallet::Wallet()
{
}

Wallet::~Wallet()
{
}

void Wallet::setMoney(int amount)
{
	this->money = amount;
}

int Wallet::getMoney() const
{
	return this->money;
}

void Wallet::addMoney(const int sum)
{
	for (int i = 0; i < sum; ++i)
	{
		// Heavy calculation simulation
		std::chrono::milliseconds time(1000);
		std::this_thread::sleep_for(time);

		// Money++
		this->money++;
		std::cout << "Worker Thread " << std::this_thread::get_id() << " is adding 1 to " << this->getMoney() << std::endl;
	}
}
