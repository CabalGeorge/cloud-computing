#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <memory>
#include "Utils.h"

auto main() -> int
{
	{
		Utils<int> lv_nUtils;
		Utils<double> lv_dfUtils;
		Utils<float> vector;

		vector.m_Vector = { 1.2, 3.5, 2.7, 10.2 };
		std::cout << "Mean of the elemenst of vector is: " << vector.meanOfVector() << std::endl;


		auto const lv_nMaxElement = lv_dfUtils.mf_MaxElement(7, 10);
		auto const lv_dfMaxElement = lv_dfUtils.mf_MaxElement(2.3, 4.5);

		std::cout << lv_nMaxElement << std::endl;
		std::cout << lv_dfMaxElement << std::endl << std::endl;
	}

	{
		std::map<int, std::string> lv_Map;
		lv_Map.insert(std::make_pair(5, std::string("FirstInsertion")));
		lv_Map.insert(std::make_pair(3, std::string("SecondInsertion")));

		for (auto const mapIterator : lv_Map)
		{
			std::cout << mapIterator.first << " " << mapIterator.second << std::endl;
		}

	}

	{
		std::map<Utils<int>, int, Utils<int>::Comparator> lv_UtilsMaps;
		Utils<int> lv_nUtils1;
		Utils<int> lv_nUtils2;
		lv_UtilsMaps.insert(std::make_pair(lv_nUtils1, 0));
		lv_UtilsMaps.insert(std::make_pair(lv_nUtils2, 1));

	}

	std::shared_ptr<Utils<int>> lv_sharedPtr1 = std::make_shared<Utils<int>>();
	{
		std::shared_ptr<Utils<int>> lv_sharedPtr2 = lv_sharedPtr1;
		std::cout << std::endl << "Refference count: " << lv_sharedPtr1.use_count() << std::endl;
	}
	std::cout << "Refference count: " << lv_sharedPtr1.use_count() << std::endl << std::endl;




	system("pause");
	return EXIT_SUCCESS;
}