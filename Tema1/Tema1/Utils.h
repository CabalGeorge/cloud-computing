#pragma once

#include <vector>

template <typename T>
class Utils
{
public:
	std::vector<T> m_Vector;

	struct Comparator
	{
		bool operator()(const Utils & left, const Utils & right) const
		{
			return left.mv_Value < right.mv_Value;
		}
	};

	Utils() = default;

	auto mf_MaxElement(T lv_FirstElement, T lv_SecondElement) const -> T
	{
		return lv_FirstElement > lv_SecondElement ? lv_FirstElement : lv_SecondElement;
	}

	T meanOfVector()
	{
		float mean = 0;

		for (int index = 0; index < m_Vector.size(); index++)
		{
			mean += m_Vector[index];
		}

		return mean / m_Vector.size();
	}

	~Utils() = default;

private:
	T mv_Value;
};
