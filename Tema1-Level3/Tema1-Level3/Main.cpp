#include"Animal.h"
#include"Dog.h"
#include"Cat.h"
#include"PetShop.h"

auto main() ->int
{
	Dog dog;
	Cat cat;
	PetShop animalMap;

	dog.name = "Rex";
	cat.name = "Flaffy";

	std::shared_ptr<Animal> dogPointer = std::make_shared<Dog>();;
	std::shared_ptr<Animal> catPointer = std::make_shared<Cat>();

	animalMap.map.insert(std::make_pair(dog, std::string("First Animal")));
	animalMap.map.insert(std::make_pair(cat, std::string("Second Animal")));


	for (auto const mapIterator : animalMap.map)
	{
		std::cout << mapIterator.first << " " << mapIterator.second << std::endl;
	}




}
