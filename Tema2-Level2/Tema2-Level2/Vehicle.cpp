#include "Vehicle.h"

Vehicle::Vehicle()
{
}

Vehicle::Vehicle(Vehicle::Brand brand = Vehicle::Brand::NONE, int year = -1, double price = 0.0f, bool fullOption = false)
	: m_brand(brand), m_year(year), m_price(price), m_isFull(fullOption)
{

}

std::ostream & operator<<(std::ostream & os, const Vehicle & other)
{
	os << " Brand: " << other.m_brand << "\n Year: " << other.m_year << "\n Full Option: " << other.m_isFull << "\n Price: " << other.m_price << "\n";

	return os;
}

Vehicle::~Vehicle()
{
}

Vehicle::Brand Vehicle::getBrand() const
{
	return m_brand;
}

int Vehicle::getYear() const
{
	return m_year;
}

double Vehicle::getPrice() const
{
	return m_price;
}

bool Vehicle::getFullOption() const
{
	return m_isFull;
}
