#include"Vehicle.h"

auto main()->int
{
	{
		unsigned int seed = std::chrono::steady_clock::now().time_since_epoch().count();
		std::default_random_engine generator(seed);
		std::uniform_int_distribution<int> brandPicks(0, 6);
		std::uniform_int_distribution<int> pricePicks(1000, 25000);
		std::uniform_int_distribution<int> yearPicks(2005, 2019);
		std::uniform_int_distribution<int> fullOptionPicks(0, 1);
		std::vector<Vehicle> vehicleVector;

		for (int index = 0; index < 5; ++index)
		{
			int brandIndex = brandPicks(generator);
			int priceIndex = pricePicks(generator);
			int yearIndex = yearPicks(generator);
			bool fullOptionIndex = fullOptionPicks(generator);

			vehicleVector.push_back(Vehicle(static_cast<Vehicle::Brand>(brandIndex), yearIndex, priceIndex, fullOptionIndex));
		}

		std::map<Vehicle, Vehicle::Brand, Vehicle::Comparator> vehicle_map;

		for (int index = 0; index < 5; ++index)
		{
			vehicle_map.insert(std::make_pair(vehicleVector[index], vehicleVector[index].getBrand()));
		}

		for (auto const mapIterator : vehicle_map)
		{
			std::cout << mapIterator.first << " " << std::endl;
		}
	}
	system("pause");
	return EXIT_SUCCESS;
}