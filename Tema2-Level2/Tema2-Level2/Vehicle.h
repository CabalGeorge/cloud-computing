#pragma once
#include<iostream>
#include<string>
#include<chrono>
#include<random>
#include<map>

class Vehicle
{
public:
	struct Comparator
	{
		bool operator()(const Vehicle& left, const Vehicle& right) const
		{
			if (std::max(left.getPrice(), right.getPrice()) - std::min(left.getPrice(), right.getPrice()) > 1000)
			{
				return left.getPrice() > right.getPrice();
			}
			else
				if (left.getFullOption() != right.getFullOption())
				{
					return left.getFullOption() > right.getFullOption();
				}
				else
				{
					return left.getYear() > right.getYear();
				}
		}
	};
	enum Brand
	{
		NONE,
		MITSUBISHI,
		MERCEDES,
		HONDA,
		SUZUKI,
		BMW
	};


	Vehicle();

	Vehicle(Brand brand, int year, double price, bool isfullOption);

	friend std::ostream& operator << (std::ostream& os, const Vehicle& other);

	virtual ~Vehicle();

	Brand getBrand() const;
	int getYear() const;
	double getPrice() const;
	bool getFullOption() const;

private:
	Brand m_brand;
	int m_year;
	double m_price;
	bool m_isFull;
};

